### How do I get set up?(on Linux) ###

* clone repository
* ```cd ias-component```
* ```cd scripts```
* run ```./prepare.sh```


### How do I run it? ###
* go to ```ias-component/scripts```
* run ```./run_sctp.sh```
* at the same time run ```./run_scweb.sh```


###After updating something to apply changes###
* go to ```ias-component/scripts```
* run ```./build_components.sh```


P.S. To stop sc_web run ```./kill_scweb.sh```