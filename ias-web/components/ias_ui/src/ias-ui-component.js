IasUiComponent = {
    ext_lang: 'ias_ui_code',
    formats: ['format_ias_ui_code_json'],

    factory: function (sandbox) {
        return new iasUiWindow(sandbox);
    }
};

var iasUiWindow = function (sandbox) {
    this.sandbox = sandbox;
    this.container = "#" + sandbox.container;
    var ui = this;

    this.receiveData = function (data) {
        console.log(data);
        var svg = d3.select('svg');
        create_new_element(svg, data || {'type': 'circle'})
    };


    var init = function () {

        var svg = d3.select('svg');
        ui.data = [ ];

        ui.drag = d3.behavior.drag()
            .origin(function (d) {
                return d;
            })
            .on("drag", dragmove);

        function dragmove(d) {
            var width = svg.style("width").replace('px', '');
            var height = svg.style("height").replace('px', '');
            switch (d.type) {
                case 'circle':
                    d3.select(this)
                        .attr("cx", function () {
                            d.x = Math.max(d.r, Math.min(width - d.r, d3.event.x));
                            return d.x;
                        })
                        .attr("cy", function () {
                            d.y = Math.max(d.r, Math.min(height - d.r, d3.event.y));
                            return d.y;
                        });
                    break;
                case 'g':
                    d3.select(this)
                        .attr("transform", "translate(" + d3.event.x + ", " + d3.event.y + ")");
                    break;
                case 'line':
                    break;
                default:
                    d3.select(this)
                        .attr("x", function () {
                            d.x = Math.max(0, Math.min(width - d.width, d3.event.x));
                            return d.x;
                        })
                        .attr("y", function () {
                            d.y = Math.max(0, Math.min(height - d.height, d3.event.y));
                            return d.y;
                        });
            }
        }


        d3.select('div.element.circle.base')
            .on('click', function () {
                svg.selectAll('.element')
                    .attr('fill', 'gray');

                create_new_element(svg, {type: 'circle'})
            });
        d3.select('div.element.rect.base')
            .on('click', function () {
                svg.selectAll('.element')
                    .attr('fill', {type: 'gray'});

                create_new_element(svg, {type: 'rectangle'})
            });
    };

    function init_element(params) {
        var coordinates = params.coordinates || [20, 20];
        var element = {
            'x': coordinates[0],
            'y': coordinates[1],
            'angle': params.angle || 0
        };
        switch (params.type) {
            case 'circle':
                element.type = 'circle';
                element.r = params.radius || 10;
                break;
            case 'rectangle':
                element.type = 'rect';
                element.width = params.width || 20;
                element.height = params.height || 20;
                break;
            case 'group':
                element.type = 'g';
                element.base = init_element(params.base);
                element.others = [];
                for (var index = 0; index < params.others.length; index++) {
                    element.others.push(init_element(params.others[index]));
                }
                break;
            case 'line':
                element.type = 'line';
                element.length = params.length;
                element.x2 = Math.cos(element.angle) * element.length + element.x;
                element.y2 = Math.sin(element.angle) * element.length + element.y;
                break;
        }
        return element;
    }

    function init_element_properties(element, element_data) {
        switch (element_data.type) {
            case 'circle':
                element
                    .attr('cx', element_data.x)
                    .attr('cy', element_data.y)
                    .attr("r", 0)
                    .transition()
                    .duration(1000)
                    .attr('r', element_data.r);
                break;
            case 'rect':
                element
                    .attr('x', element_data.x)
                    .attr('y', element_data.y)
                    .attr("width", 1)
                    .attr("heigth", 1)
                    .transition()
                    .duration(1000)
                    .attr('width', element_data.width)
                    .attr('height', element_data.height);
                break;
            case 'line':
                element
                    .attr('x1', element_data.x)
                    .attr('y1', element_data.y)
                    .attr('x2', 0)
                    .attr('y2', 0)
                    .transition()
                    .duration(1000)
                    .attr('x2', element_data.x2)
                    .attr('y2', element_data.y2);
                break;
        }
    }


    function init_group_properties(element, element_data) {
        var base = element_data.base;
        var others = element_data.others;

        var tmp = element
            .append(base.type);

        init_element_properties(tmp, base);


        for (var index = 0; index < element_data.others.length; index++) {
            var el = element_data.others[index];
            tmp = element
                .append(el.type);
            init_element_properties(tmp, el);
        }

    }

    function create_new_element(svg, params) {
        var new_element = init_element(params);
        ui.data.push(new_element);

        svg.selectAll('.element')
            .attr('fill', 'gray');

        var elements = svg.selectAll('.element')
            .data(ui.data);

            var base_element = elements
                .enter()
                .append(new_element.type)
                .attr('class', function (d) {
                    return d3.select(this).attr("class") + ' element ' + d.type;
                })
                .attr('fill', function (d) {
                    return 'red';
                })
                .call(ui.drag);
        if (params.type != 'group') {
            init_element_properties(base_element, new_element);
        } else {
            init_group_properties(base_element, new_element);
        }
            elements.exit()
                .remove();

    }

    $(this.container).load('static/components/html/ias-component.html', function () {
        init();
    });


    this.sandbox.eventDataAppend = $.proxy(this.receiveData, this);
    this.sandbox.updateContent();
};


SCWeb.core.ComponentManager.appendComponentInitialize(IasUiComponent);
